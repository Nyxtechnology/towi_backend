from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth
from users.views import (
    Login, Home, DataInformation, AddPatient,
    followPatient, RegisterPatient, Groups, EditInfoPacient,
    InviteParent, Reports, Patients, email_exists, Invitations,
    Seguimiento, get_graph, reports_redirect
)
from rest_framework_jwt.views import verify_jwt_token

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', Login.as_view(), name='login'),
    url(r'^home/', Home.as_view()),
    url(r'^information/', DataInformation.as_view()),
    url(r'^patients/', Patients.as_view()),
    url(r'^seguimiento/', Seguimiento.as_view()),
    url(r'^invitaciones/', Invitations.as_view()),
    url(r'^follow-patient/', followPatient.as_view()),
    url(r'^validate_email/', email_exists),
    url(r'^pruebas/(?P<id>\d+)$', reports_redirect),
    url(r'^get_graph/', get_graph),
    url(r'^register-patient/', RegisterPatient.as_view()),
    url(r'^invite-parent/(?P<id>\d+)$', InviteParent.as_view(), name='invite-parent'),  # NOQA
    url(r'^edit-info/(?P<id>\d+)$', EditInfoPacient.as_view(), name='edit-info'),  # NOQA
    url(r'^reports/(?P<id>\d+)$', Reports.as_view(), name='reports'),  # NOQA
    url(r'^add-patient/', AddPatient.as_view()),
    url(r'^logout/', auth.logout, {'next_page': '/'}, name='logout'),
    url(r'^groups/', Groups.as_view()),
    url(r'^api-token-verify/', verify_jwt_token),
 ]
