from django.db import models


class CancelData(models.Model):
    pid = models.IntegerField()
    id_cancelado = models.IntegerField()
    nombre = models.CharField(max_length=100)
    mail = models.CharField(max_length=100)
    id_pago_recurrente = models.IntegerField()
    motivo = models.CharField(max_length=200)
    message = models.CharField(max_length=100)

    class Meta:
        db_table = 'cancel_data'
        app_label = 'payments'


class Catalogue(models.Model):
    recurrency_id = models.IntegerField()
    max_kids = models.IntegerField()
    description = models.CharField(max_length=100)
    price = models.IntegerField()
    status = models.SmallIntegerField()
    promocode = models.CharField(max_length=45)

    class Meta:
        db_table = 'catalogue'
        app_label = 'payments'


class Recurrency(models.Model):
    recurrency_id_banwire = models.IntegerField()
    time = models.CharField(max_length=45)
    active = models.BinaryField()

    class Meta:
        db_table = 'recurrency'
        app_label = 'payments'


class SuscriptionData(models.Model):
    pid = models.IntegerField()
    type = models.IntegerField()
    card_name = models.CharField(max_length=100)
    card_lastname = models.CharField(max_length=100)
    card_email = models.CharField(max_length=100)
    id_transaccion = models.CharField(max_length=200)
    token = models.CharField(max_length=200)
    id_tarjeta = models.CharField(max_length=200)
    id_pago_recurrente = models.CharField(max_length=200)
    code = models.IntegerField()
    fecha_de_inicio = models.DateTimeField()
    recurrencia = models.IntegerField()
    success = models.IntegerField()
    blocked = models.IntegerField()
    canceled = models.IntegerField()
    monto = models.FloatField()
    message = models.CharField(max_length=200)

    class Meta:
        db_table = 'suscription_data'
        app_label = 'payments'
