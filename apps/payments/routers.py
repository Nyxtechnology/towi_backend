class PaymentsRouter(object):
    """
    A router to control all database operations on models in the
    payments application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read payments models go to payments.
        """
        if model._meta.app_label == 'payments':
            return 'paymentsDB'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write payments models go to paymentsDB.
        """
        if model._meta.app_label == 'payments':
            return 'paymentsDB'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the payments app is involved.
        """
        if obj1._meta.app_label == 'payments' or \
           obj2._meta.app_label == 'payments':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the payments app only appears in the 'paymentsDB'
        database.
        """
        if app_label == 'payments':
            return db == 'paymentsDB'
        else:
            return db == 'default'
        return None
