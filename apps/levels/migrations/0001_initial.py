# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-27 21:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ArbolmusicalHeaders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('gametime', models.IntegerField(db_column='gameTime')),
                ('passedlevels', models.IntegerField(db_column='passedLevels')),
                ('repeatedlevels', models.IntegerField(db_column='repeatedLevels')),
                ('playedlevels', models.IntegerField(db_column='playedLevels')),
            ],
            options={
                'db_table': 'arbolmusical_headers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ArbolmusicalLevels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headerid', models.IntegerField()),
                ('birds', models.IntegerField()),
                ('nests', models.IntegerField()),
                ('level', models.IntegerField()),
                ('sublevel', models.IntegerField()),
                ('tutorial', models.IntegerField()),
                ('sound', models.CharField(max_length=500)),
                ('birdsound', models.CharField(db_column='birdSound', max_length=500)),
                ('time', models.IntegerField()),
                ('birdlistenedpre', models.CharField(db_column='birdListenedPre', max_length=500)),
                ('birdlistened', models.CharField(db_column='birdListened', max_length=500)),
                ('errors', models.IntegerField()),
                ('correct', models.IntegerField()),
            ],
            options={
                'db_table': 'arbolmusical_levels',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ArenamagicaHeaders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('gametime', models.IntegerField(db_column='gameTime')),
                ('passedlevels', models.IntegerField(db_column='passedLevels')),
                ('repeatedlevels', models.IntegerField(db_column='repeatedLevels')),
                ('playedlevels', models.IntegerField(db_column='playedLevels')),
            ],
            options={
                'db_table': 'arenamagica_headers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ArenamagicaLevels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headerid', models.IntegerField()),
                ('levelkey', models.CharField(max_length=100)),
                ('level', models.IntegerField()),
                ('sublevel', models.IntegerField()),
                ('time', models.IntegerField()),
                ('passed', models.IntegerField()),
                ('repeated', models.IntegerField()),
                ('accuracy', models.IntegerField()),
            ],
            options={
                'db_table': 'arenamagica_levels',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ChildrenTowiIsland',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('kiwis', models.IntegerField()),
                ('avatar', models.CharField(max_length=100)),
                ('avatarclothes', models.CharField(db_column='avatarClothes', max_length=1000)),
                ('owneditems', models.CharField(db_column='ownedItems', max_length=1000)),
                ('activemissions', models.CharField(db_column='activeMissions', max_length=1000)),
                ('missionlist', models.CharField(db_column='missionList', max_length=1000)),
                ('activeday', models.IntegerField(db_column='activeDay')),
                ('riotutorial', models.IntegerField(db_column='rioTutorial')),
                ('tesorotutorial', models.IntegerField(db_column='tesoroTutorial')),
                ('arbolmusicaltutorial', models.IntegerField(db_column='arbolMusicalTutorial')),
            ],
            options={
                'db_table': 'children_towi_island',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DondequedolabolitaHeaders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('gametime', models.IntegerField(db_column='gameTime')),
                ('passedlevels', models.IntegerField(db_column='passedLevels')),
                ('repeatedlevels', models.IntegerField(db_column='repeatedLevels')),
                ('playedlevels', models.IntegerField(db_column='playedLevels')),
            ],
            options={
                'db_table': 'dondequedolabolita_headers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DondequedolabolitaLevels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headerid', models.IntegerField()),
                ('levelkey', models.CharField(db_column='levelKey', max_length=100)),
                ('level', models.IntegerField()),
                ('sublevel', models.IntegerField()),
                ('time', models.IntegerField()),
                ('numofmonkeys', models.IntegerField(db_column='numOfMonkeys')),
                ('numofobjects', models.IntegerField(db_column='numOfObjects')),
                ('instructions', models.CharField(max_length=500)),
                ('numofmovements', models.IntegerField(db_column='numOfMovements')),
                ('timeofmovements', models.CharField(db_column='timeOfMovements', max_length=100)),
                ('correct', models.IntegerField()),
            ],
            options={
                'db_table': 'dondequedolabolita_levels',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='GameAreas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('game_id', models.IntegerField()),
                ('area', models.IntegerField()),
                ('subarea', models.IntegerField()),
            ],
            options={
                'db_table': 'game_areas',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='GamesConfiguration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cid', models.IntegerField()),
                ('configid', models.IntegerField(db_column='configID')),
                ('sid', models.IntegerField()),
                ('date', models.DateTimeField()),
            ],
            options={
                'db_table': 'games_configuration',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='GamesConfigurationFiles',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=1000)),
                ('file', models.CharField(max_length=100)),
                ('date', models.DateTimeField()),
            ],
            options={
                'db_table': 'games_configuration_files',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PixframeCardsCodes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=100)),
                ('used', models.IntegerField()),
                ('temp', models.IntegerField()),
                ('temp_num', models.IntegerField()),
                ('activation_day', models.DateField()),
            ],
            options={
                'db_table': 'pixframe_cards_codes',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PruebaConosDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headerid', models.IntegerField(db_column='headerId')),
                ('cono', models.IntegerField()),
                ('distance', models.FloatField()),
                ('time', models.IntegerField()),
                ('state', models.CharField(max_length=200)),
                ('order', models.IntegerField()),
            ],
            options={
                'db_table': 'prueba_conos_details',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PruebaConosHeaders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=500)),
                ('age', models.CharField(max_length=50)),
                ('sex', models.CharField(max_length=50)),
                ('routeid', models.CharField(db_column='routeId', max_length=50)),
                ('testdate', models.DateTimeField(db_column='testDate')),
            ],
            options={
                'db_table': 'prueba_conos_headers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PruebasHeaders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('gametime', models.IntegerField(db_column='gameTime')),
                ('passedlevels', models.IntegerField(db_column='passedLevels')),
                ('repeatedlevels', models.IntegerField(db_column='repeatedLevels')),
                ('playedlevels', models.IntegerField(db_column='playedLevels')),
            ],
            options={
                'db_table': 'pruebas_headers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PruebasHeadersNew',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('gametime', models.IntegerField(db_column='gameTime')),
                ('passedlevels', models.IntegerField(db_column='passedLevels')),
                ('repeatedlevels', models.IntegerField(db_column='repeatedLevels')),
                ('playedlevels', models.IntegerField(db_column='playedLevels')),
            ],
            options={
                'db_table': 'pruebas_headers_new',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PruebasLevels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header_id', models.IntegerField()),
                ('playerage', models.IntegerField(db_column='playerAge')),
                ('birthdayofplayer', models.CharField(db_column='birthdayOfPlayer', max_length=300)),
                ('inputagetimeofcomp', models.IntegerField(db_column='inputAgeTimeOfComp')),
                ('playername', models.CharField(db_column='playerName', max_length=300)),
                ('playeraddress', models.CharField(db_column='playerAddress', max_length=300)),
                ('currentdate', models.CharField(db_column='currentDate', max_length=300)),
                ('buytickettimeofcomp', models.IntegerField(db_column='buyTicketTimeOfComp')),
                ('normalpackscore', models.IntegerField(db_column='normalPackScore')),
                ('reversepackscore', models.IntegerField(db_column='reversePackScore')),
                ('packtimeofcomp', models.IntegerField(db_column='packTimeOfComp')),
                ('packtypeofweather', models.CharField(db_column='packTypeOfWeather', max_length=300)),
                ('weatherobjectpacked', models.CharField(db_column='weatherObjectPacked', max_length=1000)),
                ('objecttoremember', models.CharField(db_column='objectToRemember', max_length=1000)),
                ('timeoflab', models.CharField(db_column='timeOfLab', max_length=1000)),
                ('latenciesoflab', models.CharField(db_column='latenciesOfLab', max_length=1000)),
                ('hitsoflab', models.CharField(db_column='hitsOfLab', max_length=1000)),
                ('totalhits', models.IntegerField(db_column='totalHits')),
                ('crossesoflab', models.CharField(db_column='crossesOfLab', max_length=1000)),
                ('totalcrosses', models.IntegerField(db_column='totalCrosses')),
                ('deadendsoflab', models.CharField(db_column='deadEndsOfLab', max_length=1000)),
                ('totaldeadends', models.IntegerField(db_column='totalDeadEnds')),
                ('airportroutetimeofcomp', models.IntegerField(db_column='airportRouteTimeOfComp')),
                ('waitroomcorrect', models.IntegerField(db_column='waitRoomCorrect')),
                ('waitroomincorrect', models.IntegerField(db_column='waitRoomIncorrect')),
                ('waitroommissed', models.IntegerField(db_column='waitRoomMissed')),
                ('timebetweenflights', models.FloatField(db_column='timeBetweenFlights')),
                ('flyplanecorrect', models.IntegerField(db_column='flyPlaneCorrect')),
                ('flyplaneincorrect', models.IntegerField(db_column='flyPlaneIncorrect')),
                ('flyplanemissed', models.IntegerField(db_column='flyPlaneMissed')),
                ('flyplanetimeforinput', models.FloatField(db_column='flyPlaneTimeForInput')),
                ('flyplanetimeofcomp', models.IntegerField(db_column='flyPlaneTimeOfComp')),
                ('pupcoinsmincorrect', models.IntegerField(db_column='pUpCoinsMinCorrect')),
                ('pupcoinsminincorrect', models.IntegerField(db_column='pUpCoinsMinIncorrect')),
                ('pupcoinsminmissed', models.IntegerField(db_column='pUpCoinsMinMissed')),
                ('pupcoinsextracorrect', models.IntegerField(db_column='pUpCoinsExtraCorrect')),
                ('pupcoinsextraincorrect', models.IntegerField(db_column='pUpCoinsExtraIncorrect')),
                ('pupcoinsextramissed', models.IntegerField(db_column='pUpCoinsExtraMissed')),
                ('coinsselected', models.CharField(db_column='coinsSelected', max_length=1000)),
                ('pupcoinstimeofcomp', models.IntegerField(db_column='pUpCoinsTimeOfComp')),
                ('unpackfirstobjs', models.CharField(db_column='unPackFirstObjs', max_length=1000)),
                ('unpackcorrectsample', models.CharField(db_column='unPackCorrectSample', max_length=1000)),
                ('unpackincorrectsample', models.CharField(db_column='unPackIncorrectSample', max_length=1000)),
                ('unpackrepeatedsample', models.CharField(db_column='unPackRepeatedSample', max_length=1000)),
                ('unpackfourfirstsample', models.CharField(db_column='unPackFourFirstSample', max_length=1000)),
                ('unpackfourlastsample', models.CharField(db_column='unPackFourLastSample', max_length=1000)),
                ('unpackgroupingsample', models.CharField(db_column='unPackGroupingSample', max_length=1000)),
                ('unpackspacialprecisionsample', models.CharField(db_column='unPackSpacialPrecisionSample', max_length=1000)),
                ('unpackpictime', models.IntegerField(db_column='unPackPicTime')),
                ('unpacktimeofcomp', models.IntegerField(db_column='unPackTimeOfComp')),
                ('unpacktotalcorrect', models.IntegerField(db_column='unPackTotalCorrect')),
            ],
            options={
                'db_table': 'pruebas_levels',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PruebasLevelsNew',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header_id', models.IntegerField()),
                ('playerage', models.IntegerField(db_column='playerAge')),
                ('playerbirthday', models.CharField(db_column='playerBirthday', max_length=300)),
                ('inputagetimeofcomp', models.IntegerField(db_column='inputAgeTimeOfComp')),
                ('playername', models.CharField(db_column='playerName', max_length=300)),
                ('playeraddress', models.CharField(db_column='playerAddress', max_length=300)),
                ('playercurrentdate', models.CharField(db_column='playerCurrentDate', max_length=300)),
                ('buytickettimeofcomp', models.IntegerField(db_column='buyTicketTimeOfComp')),
                ('packnormalscore', models.IntegerField(db_column='packNormalScore')),
                ('packreversescore', models.IntegerField(db_column='packReverseScore')),
                ('packtimeofcomp', models.IntegerField(db_column='packTimeOfComp')),
                ('weatherpacktype', models.CharField(db_column='weatherPackType', max_length=300)),
                ('weatherobjectpacked', models.CharField(db_column='weatherObjectPacked', max_length=1000)),
                ('weathercongruence', models.IntegerField(db_column='weatherCongruence')),
                ('unpacktoremember', models.CharField(db_column='unPackToRemember', max_length=1000)),
                ('lab1time', models.FloatField(db_column='lab1Time')),
                ('lab2time', models.FloatField(db_column='lab2Time')),
                ('lab3time', models.FloatField(db_column='lab3Time')),
                ('lab1latency', models.FloatField(db_column='lab1Latency')),
                ('lab2latency', models.FloatField(db_column='lab2Latency')),
                ('lab3latency', models.FloatField(db_column='lab3Latency')),
                ('lab1hits', models.IntegerField(db_column='lab1Hits')),
                ('lab2hits', models.IntegerField(db_column='lab2Hits')),
                ('lab3hits', models.IntegerField(db_column='lab3Hits')),
                ('lab1crosses', models.IntegerField(db_column='lab1Crosses')),
                ('lab2crosses', models.IntegerField(db_column='lab2Crosses')),
                ('lab3crosses', models.IntegerField(db_column='lab3Crosses')),
                ('lab1deadends', models.IntegerField(db_column='lab1DeadEnds')),
                ('lab2deadends', models.IntegerField(db_column='lab2DeadEnds')),
                ('lab3deadends', models.IntegerField(db_column='lab3DeadEnds')),
                ('labtotalhits', models.IntegerField(db_column='labTotalHits')),
                ('labtotalcrosses', models.IntegerField(db_column='labTotalCrosses')),
                ('labtotaldeadends', models.IntegerField(db_column='labTotalDeadEnds')),
                ('labxhits', models.DecimalField(db_column='labXHits', decimal_places=2, max_digits=4)),
                ('labxcrosses', models.DecimalField(db_column='labXCrosses', decimal_places=2, max_digits=4)),
                ('labxdeadends', models.DecimalField(db_column='labXDeadEnds', decimal_places=2, max_digits=4)),
                ('labtimeofcomp', models.IntegerField(db_column='labTimeOfComp')),
                ('waitroomcorrect', models.IntegerField(db_column='waitRoomCorrect')),
                ('waitroomincorrect', models.IntegerField(db_column='waitRoomIncorrect')),
                ('waitroommissed', models.IntegerField(db_column='waitRoomMissed')),
                ('timebetweenflights', models.FloatField(db_column='timeBetweenFlights')),
                ('flyplanecorrect', models.IntegerField(db_column='flyPlaneCorrect')),
                ('flyplaneincorrect', models.IntegerField(db_column='flyPlaneIncorrect')),
                ('flyplanemissed', models.IntegerField(db_column='flyPlaneMissed')),
                ('flyplanegreencorrect', models.IntegerField(db_column='flyPlaneGreenCorrect')),
                ('flyplanegreenincorrect', models.IntegerField(db_column='flyPlaneGreenIncorrect')),
                ('flyplanegreenmissed', models.IntegerField(db_column='flyPlaneGreenMissed')),
                ('flyplanetimeforinput', models.FloatField(db_column='flyPlaneTimeForInput')),
                ('flyplanetimeofcomp', models.IntegerField(db_column='flyPlaneTimeOfComp')),
                ('coinsmincorrect', models.IntegerField(db_column='coinsMinCorrect')),
                ('coinsminincorrect', models.IntegerField(db_column='coinsMinIncorrect')),
                ('coinsminmissed', models.IntegerField(db_column='coinsMinMissed')),
                ('coinsextracorrect', models.IntegerField(db_column='coinsExtraCorrect')),
                ('coinsextraincorrect', models.IntegerField(db_column='coinsExtraIncorrect')),
                ('coinsextramissed', models.IntegerField(db_column='coinsExtraMissed')),
                ('coinspatterntype', models.IntegerField(db_column='coinsPatternType')),
                ('coinsselected', models.CharField(db_column='coinsSelected', max_length=1000)),
                ('coinsclicksbeforemin', models.IntegerField(db_column='coinsClicksBeforeMin')),
                ('coinstimeofcomp', models.IntegerField(db_column='coinsTimeOfComp')),
                ('unpackfirstselected', models.CharField(db_column='unPackFirstSelected', max_length=1000)),
                ('unpackfirstcorrect', models.IntegerField(db_column='unPackFirstCorrect')),
                ('unpackfirstperc', models.DecimalField(db_column='unPackFirstPerc', decimal_places=2, max_digits=4)),
                ('unpacks1correct', models.IntegerField(db_column='unPackS1Correct')),
                ('unpacks2correct', models.IntegerField(db_column='unPackS2Correct')),
                ('unpacks3correct', models.IntegerField(db_column='unPackS3Correct')),
                ('unpacks1incorrect', models.IntegerField(db_column='unPackS1Incorrect')),
                ('unpacks2incorrect', models.IntegerField(db_column='unPackS2Incorrect')),
                ('unpacks3incorrect', models.IntegerField(db_column='unPackS3Incorrect')),
                ('unpacks1repeated', models.IntegerField(db_column='unPackS1Repeated')),
                ('unpacks2repeated', models.IntegerField(db_column='unPackS2Repeated')),
                ('unpacks3repeated', models.IntegerField(db_column='unPackS3Repeated')),
                ('unpackfourfirstsample', models.CharField(db_column='unPackFourFirstSample', max_length=1000)),
                ('unpackfourlastsample', models.CharField(db_column='unPackFourLastSample', max_length=1000)),
                ('unpackgroupingsample', models.CharField(db_column='unPackGroupingSample', max_length=1000)),
                ('unpackspacialprecisionsample', models.CharField(db_column='unPackSpacialPrecisionSample', max_length=1000)),
                ('unpackpictime', models.IntegerField(db_column='unPackPicTime')),
                ('unpacktimeofcomp', models.IntegerField(db_column='unPackTimeOfComp')),
                ('unpacktotalcorrect', models.IntegerField(db_column='unPackTotalCorrect')),
                ('unpackxtotalcorrect', models.DecimalField(db_column='unPackXTotalCorrect', decimal_places=2, max_digits=4)),
                ('unpackperctotalcorrect', models.DecimalField(db_column='unPackPercTotalCorrect', decimal_places=2, max_digits=4)),
                ('unpacktotalincorrect', models.IntegerField(db_column='unPackTotalIncorrect')),
                ('unpackxtotalincorrect', models.DecimalField(db_column='unPackXTotalIncorrect', decimal_places=2, max_digits=4)),
                ('unpackperctotalincorrect', models.DecimalField(db_column='unPackPercTotalIncorrect', decimal_places=2, max_digits=4)),
            ],
            options={
                'db_table': 'pruebas_levels_new',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='RecolecciondeltesoroHeaders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('gametime', models.IntegerField(db_column='gameTime')),
                ('passedlevels', models.IntegerField(db_column='passedLevels')),
                ('repeatedlevels', models.IntegerField(db_column='repeatedLevels')),
                ('playedlevels', models.IntegerField(db_column='playedLevels')),
            ],
            options={
                'db_table': 'recolecciondeltesoro_headers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='RecolecciondeltesoroLevels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headerid', models.IntegerField()),
                ('level', models.IntegerField()),
                ('sublevel', models.IntegerField()),
                ('time', models.IntegerField()),
                ('tutorial', models.IntegerField()),
                ('passed', models.IntegerField()),
                ('playerobjects', models.CharField(db_column='playerObjects', max_length=500)),
                ('playerobjectsquantity', models.CharField(db_column='playerObjectsQuantity', max_length=500)),
                ('correctobjects', models.CharField(db_column='correctObjects', max_length=500)),
                ('correctobjectsquantity', models.CharField(db_column='correctObjectsQuantity', max_length=500)),
                ('spawnedobjects', models.CharField(db_column='spawnedObjects', max_length=500)),
                ('spawneddistractors', models.CharField(db_column='spawnedDistractors', max_length=500)),
                ('notsurecorrect', models.IntegerField(db_column='notSureCorrect')),
                ('notsureincorrect', models.IntegerField(db_column='notSureIncorrect')),
                ('minobjects', models.IntegerField(db_column='minObjects')),
                ('maxobjects', models.IntegerField(db_column='maxObjects')),
                ('availableobjects', models.CharField(db_column='availableObjects', max_length=500)),
                ('availablecategories', models.CharField(db_column='availableCategories', max_length=500)),
                ('searchorders', models.CharField(db_column='searchOrders', max_length=500)),
                ('availabledistractors', models.CharField(db_column='availableDistractors', max_length=500)),
            ],
            options={
                'db_table': 'recolecciondeltesoro_levels',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='RioHeaders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('gametime', models.IntegerField(db_column='gameTime')),
                ('passedlevels', models.IntegerField(db_column='passedLevels')),
                ('repeatedlevels', models.IntegerField(db_column='repeatedLevels')),
                ('playedlevels', models.IntegerField(db_column='playedLevels')),
            ],
            options={
                'db_table': 'rio_headers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='RioLevels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headerid', models.IntegerField()),
                ('level', models.IntegerField()),
                ('sublevel', models.IntegerField()),
                ('time', models.IntegerField()),
                ('tutorial', models.IntegerField()),
                ('reverse', models.IntegerField()),
                ('speed', models.IntegerField()),
                ('correctobjects', models.IntegerField(db_column='correctObjects')),
                ('incorrectobjects', models.IntegerField(db_column='incorrectObjects')),
                ('levelobjects', models.CharField(db_column='levelObjects', max_length=500)),
                ('availableobjects', models.CharField(db_column='availableObjects', max_length=500)),
                ('reverseobjects', models.CharField(db_column='reverseObjects', max_length=500)),
                ('neutralobjects', models.CharField(db_column='neutralObjects', max_length=500)),
                ('forceforestobjects', models.CharField(db_column='forceForestObjects', max_length=500)),
                ('forcebeachforest', models.CharField(db_column='forceBeachForest', max_length=500)),
                ('specialreverseobjects', models.CharField(db_column='specialReverseObjects', max_length=500)),
                ('specialleaveobjects', models.CharField(db_column='specialLeaveObjects', max_length=500)),
            ],
            options={
                'db_table': 'rio_levels',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ShadowsHeaders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('gametime', models.IntegerField(db_column='gameTime')),
                ('passedlevels', models.IntegerField(db_column='passedLevels')),
                ('repeatedlevels', models.IntegerField(db_column='repeatedLevels')),
                ('playedlevels', models.IntegerField(db_column='playedLevels')),
            ],
            options={
                'db_table': 'shadows_headers',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ShadowsLevels',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('headerid', models.IntegerField()),
                ('levelkey', models.CharField(db_column='levelKey', max_length=100)),
                ('level', models.IntegerField()),
                ('sublevel', models.IntegerField()),
                ('shadow', models.CharField(max_length=100)),
                ('shadowtime', models.CharField(db_column='shadowTime', max_length=100)),
                ('numofoptions', models.IntegerField(db_column='numOfOptions')),
                ('options', models.CharField(max_length=500)),
                ('correct', models.IntegerField()),
                ('time', models.IntegerField()),
            ],
            options={
                'db_table': 'shadows_levels',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TowiIndex',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('gamekey', models.CharField(db_column='gameKey', max_length=100)),
                ('index', models.FloatField()),
                ('date', models.DateTimeField()),
                ('serverdate', models.DateTimeField(db_column='serverDate')),
            ],
            options={
                'db_table': 'towi_index',
                'managed': False,
            },
        ),
    ]
