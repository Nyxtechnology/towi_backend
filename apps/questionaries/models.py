from django.db import models


class QuestionarieAnswers(models.Model):
    status_id = models.IntegerField()
    pregunta = models.CharField(max_length=500)
    respuesta = models.CharField(max_length=500)

    class Meta:
        managed = False
        db_table = 'questionarie_answers'


class QuestionarieAnswersOld(models.Model):
    status_id = models.IntegerField()
    pregunta = models.CharField(max_length=500)
    respuesta = models.CharField(max_length=500)

    class Meta:
        managed = False
        db_table = 'questionarie_answers_old'


class QuestionarieParents(models.Model):
    pid = models.IntegerField()
    pregunta = models.CharField(max_length=500)
    respuesta = models.CharField(max_length=500)

    class Meta:
        managed = False
        db_table = 'questionarie_parents'


class QuestionariePostAnswers(models.Model):
    status_id = models.IntegerField()
    pregunta = models.CharField(max_length=500)
    respuesta = models.CharField(max_length=500)

    class Meta:
        managed = False
        db_table = 'questionarie_post_answers'


class QuestionariePostStatus(models.Model):
    id = models.AutoField(unique=True)
    parent_id = models.IntegerField()
    child_id = models.IntegerField(primary_key=True)
    status = models.IntegerField()
    status_post = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'questionarie_post_status'


class QuestionarieStatus(models.Model):
    id = models.AutoField(unique=True)
    parent_id = models.IntegerField()
    child_id = models.IntegerField(primary_key=True)
    status = models.IntegerField()
    status_post = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'questionarie_status'
