from users.models import *
import sys

from django.core.management.base import BaseCommand
from datetime import datetime

# reload(sys)
# sys.setdefaultencoding('utf8')

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        for child in Children.objects.all():
            try:
                if child.parentid is not -1 and child.user is None:
                    child.user = User.objects.get(related_user=child.parentid)
                    child.save()
            except Exception:
                print(child.id)
        self.stdout.write(self.style.SUCCESS('Successfully user and child creation'))
