from users.models import Children
import sys

from django.core.management.base import BaseCommand
from datetime import datetime

# reload(sys)
# sys.setdefaultencoding('utf8')


class Command(BaseCommand):
    """
    Comando para quitar todos las fecha que tengan null
    """
    def handle(self, *args, **options):
        count = 0
        for child in Children.objects.all():
            if child.dob in ['', ' ', None, '00-00-0000', 'Fecha de nacimiento', 'Fecha de nacimiento010101', 'Fecha de nacimiento08-12-2007', '21-12- 2006', '24/Feb/2006', '2017-10-01', '2017-10-02', '2017-10-03', '2017-10-06', '11 11 2002', '12 12 2006']:
                fecha = datetime.today()
                fecha = fecha.strftime('%Y-%m-%d')
                child.dob = fecha
                count += 1
                self.stdout.write('se agregaron nuevaas fechas')
            child.dob = child.dob.replace('/', '-')
            try:
                fecha = datetime.strptime(child.dob, "%m-%d-%Y").date()
                fecha = fecha.strftime('%Y-%m-%d')
                child.dob = fecha
                self.stdout.write('se actualizo fecha')
                child.save()
                continue
            except Exception:
                pass
            try:
                fecha = datetime.strptime(child.dob, "%d-%m-%Y").date()
                fecha = fecha.strftime('%Y-%m-%d')
                child.dob = fecha
                self.stdout.write('se actualizo fecha')
                child.save()
                continue
            except Exception:
                pass
            try:
                fecha = datetime.strptime(child.dob, "%Y/%m/%d").date()
                fecha = fecha.strftime('%Y-%m-%d')
                child.dob = fecha
                self.stdout.write('se actualizo fecha')
                child.save()
                continue
            except Exception:
                pass
            try:
                if child.dob == '00-00-0000':
                    fecha = datetime.today()
                    fecha = fecha.strftime('%Y-%m-%d')
                    child.dob = fecha
                    self.stdout.write('se actualizo fecha')
            except Exception:
                pass
            child.save()

        self.stdout.write('se agragaron nuevas fechas {} veces'.format(count))
