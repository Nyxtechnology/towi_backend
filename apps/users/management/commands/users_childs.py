from users.models import *
import sys

from django.core.management.base import BaseCommand
from datetime import datetime

# reload(sys)
# sys.setdefaultencoding('utf8')

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        django_users = []
        for user in User.objects.all():
            if user.related_user:
                django_users.append(user.related_user.id)
        users = Users.objects.exclude(id__in=django_users)
        for user in users:
            try:
                u = User.objects.create(
                    related_user=user,
                    email=user.email,
                )
                if user.name:
                    u.first_name = user.name
                if user.lastname:
                    u.last_name = user.lastname
                if user.user_type:
                    try:
                        u.user_type = UserType.objects.get(pk=user.user_type)
                    except Exception:
                        print(user.id)
            except Exception:
                print(user.id)
        self.stdout.write(self.style.SUCCESS('Successfully user and child creation'))
