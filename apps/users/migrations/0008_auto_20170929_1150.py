# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-29 16:50
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_user_user_type'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='children',
            options={},
        ),
    ]
