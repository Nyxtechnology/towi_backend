# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-27 21:29
from __future__ import unicode_literals

import django.contrib.auth.models
import django.contrib.auth.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='AccountInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pid', models.IntegerField()),
                ('acc_type', models.CharField(max_length=100)),
                ('activation_code', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'account_info',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='AccountType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=45)),
            ],
            options={
                'db_table': 'account_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Apikeys',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parentid', models.IntegerField()),
                ('key', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'apikeys',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Children',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parentid', models.IntegerField()),
                ('name', models.CharField(max_length=100)),
                ('lastname', models.CharField(max_length=100)),
                ('dob', models.CharField(max_length=100)),
                ('grade', models.CharField(max_length=100)),
                ('genre', models.CharField(max_length=100)),
                ('school', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('picture', models.CharField(max_length=100)),
                ('register_date', models.DateTimeField()),
                ('active', models.IntegerField()),
                ('trial_active', models.IntegerField()),
            ],
            options={
                'db_table': 'children',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ChildrenTowiIsland',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('parentid', models.IntegerField(db_column='parentId')),
                ('cid', models.IntegerField()),
                ('kiwis', models.IntegerField()),
                ('avatar', models.CharField(max_length=100)),
                ('avatarclothes', models.CharField(db_column='avatarClothes', max_length=1000)),
                ('owneditems', models.CharField(db_column='ownedItems', max_length=1000)),
                ('activemissions', models.CharField(db_column='activeMissions', max_length=1000)),
                ('missionlist', models.CharField(db_column='missionList', max_length=1000)),
                ('activeday', models.IntegerField(db_column='activeDay')),
                ('riotutorial', models.IntegerField(db_column='rioTutorial')),
                ('tesorotutorial', models.IntegerField(db_column='tesoroTutorial')),
                ('arbolmusicaltutorial', models.IntegerField(db_column='arbolMusicalTutorial')),
            ],
            options={
                'db_table': 'children_towi_island',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Institutions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('type', models.CharField(max_length=45)),
                ('country', models.CharField(max_length=45)),
                ('state', models.CharField(max_length=45)),
                ('website', models.CharField(max_length=45)),
                ('contactname', models.CharField(db_column='contactName', max_length=45)),
                ('contactlastname', models.CharField(db_column='contactLastname', max_length=45)),
                ('contactemail', models.CharField(max_length=45)),
                ('contactphone', models.CharField(db_column='contactPhone', max_length=45)),
            ],
            options={
                'db_table': 'institutions',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Specialists',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('lastname', models.CharField(max_length=100)),
                ('country', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('genre', models.CharField(max_length=100)),
                ('relation', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('picture', models.CharField(max_length=100)),
                ('active', models.IntegerField()),
                ('verified', models.IntegerField()),
                ('dob', models.CharField(max_length=100)),
                ('register_date', models.DateTimeField()),
                ('fb_id', models.CharField(max_length=200)),
                ('trial_active', models.IntegerField()),
                ('public', models.IntegerField()),
            ],
            options={
                'db_table': 'specialists',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('lastname', models.CharField(max_length=100)),
                ('country', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('genre', models.CharField(max_length=100)),
                ('relation', models.CharField(max_length=100)),
                ('email', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('picture', models.CharField(max_length=100)),
                ('dob', models.DateField()),
                ('verified', models.IntegerField()),
                ('register_date', models.DateTimeField()),
                ('fb_id', models.CharField(max_length=200)),
                ('user_type', models.IntegerField()),
                ('account_type', models.IntegerField()),
            ],
            options={
                'db_table': 'users',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='UserSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.IntegerField()),
                ('session_type', models.IntegerField()),
                ('date', models.DateTimeField()),
            ],
            options={
                'db_table': 'user_session',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='UserType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=45)),
            ],
            options={
                'db_table': 'user_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=30, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=30, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('related_user', models.OneToOneField(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='usuarios', to='users.Users')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name_plural': 'Authenticate users',
                'verbose_name': 'Authenticate user',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
    ]
