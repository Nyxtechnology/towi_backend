from django.contrib import admin
from .models import Users, User, UserType, Group, Children, LinkedAccounts, LinkedAccountsOptions, ValidationIds
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin


class UserAdmin(BaseUserAdmin):
    list_display = ['email']
    ordering = ('email', )
    list_filter = ('email', )
    readonly_fields = ('date_joined', )
    fieldsets = (
        ('Informacion Persoanl', {
            'classes': ('wide',),
            'fields': (
                'first_name', 'last_name', 'email', 'password'
            )
        }),
        ('Usuario', {
            'classes': ('wide',),
            'fields': ('related_user',)
        }),
        ('Permisos', {
            'classes': ('wide',),
            'fields': ('is_active', 'is_staff', 'groups', 'user_permissions')
        }),
        ('Informacion importante', {
            'classes': ('wide', ),
            'fields': ('last_login', 'date_joined')
        })
    )
    add_fieldsets = (
        ('Informacion Persoanl', {
            'classes': ('wide',),
            'fields': (
                'first_name', 'last_name', 'email', 'password'
            )
        }),
        ('Usuario', {
            'classes': ('wide',),
            'fields': ('related_user',)
        }),
        ('Permisos', {
            'classes': ('wide',),
            'fields': ('is_active', 'is_staff', 'groups', 'user_permissions')
        }),
        ('Informacion importante', {
            'classes': ('wide', ),
            'fields': ('last_login', 'date_joined')
        })
    )


class adminUserType(admin.ModelAdmin):
    list_display = ('id', 'description')

admin.site.register(Users)
admin.site.register(User, UserAdmin)
admin.site.register(UserType, adminUserType)
admin.site.register(Group)
admin.site.register(LinkedAccounts)
admin.site.register(ValidationIds)
admin.site.register(LinkedAccountsOptions)
