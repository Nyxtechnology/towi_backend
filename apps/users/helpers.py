import string
import random
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from rest_framework_jwt.settings import api_settings
from .models import ValidationIds


def create_vinculationCode():
    unique = False
    while unique is False:
        code = ''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(40)
        )
        if not ValidationIds.objects.filter(vcode=code).exists():
            return code
        else:
            unique = False


def create_linkId():
    unique = False
    while unique is False:
        code = ''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(5)
        )
        if not ValidationIds.objects.filter(link_id=code).exists():
            return 'PXL' + code
        else:
            unique = False


class EmailBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        try:
            user = UserModel.objects.get(email=username)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None


def generate_jwt(user):
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token
