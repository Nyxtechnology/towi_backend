# -*- coding: utf-8 -*-
import datetime
import os
import mimetypes
from django.conf import settings
from django.core.files.storage import Storage
from azure.storage import BlobService
from django.core.files.base import ContentFile
from django.utils.deconstruct import deconstructible
from datetime import datetime


credentials = {
    'blobAccount': 'towistorage',
    'blobContainer': 'media',
    'blobKey': '9t3WDutHqa1dIxp62s7+zjn3SjpTcClc1ioBtpo9RBH3unssaNeV399vFDbKBIakgMMNMvzG/u95QIsRnJzEPA=='  # NOQA
}

baseStorageUri = "https://{blobAccount}.blob.core.windows.net/{blobContainer}/".format(**credentials)  # NOQA


@deconstructible
class AzureBlobStorage(Storage):

    def __init__(self, container=None):
        self.blob_service = BlobService(account_name=credentials['blobAccount'], account_key=credentials['blobKey'])  # NOQA
        if container is not None:
            self.container = credentials['blobContainer']
        else:
            self.container = container
        self.blob_service.create_container(self.container, x_ms_blob_public_access='container')  # NOQA

    def _open(self, name, mode='rb'):
        data = self.blob_service.get_blob(self.container, name)
        return ContentFile(data)

    def get_valid_name(self, name):
        return name

    def get_available_name(self, name, max_length=None):
        return name

    def _save(self, name, content):
        # content.open(mode="rb")
        myblob = content.read()
        content_type = mimetypes.guess_type(name)[0]
        myblobname = make_readable_name(content.name)
        metadata = {"modified_time": "%s" % datetime.now()}
        self.blob_service.put_block_blob_from_bytes(self.container, myblobname, myblob, x_ms_blob_content_type=content_type, x_ms_meta_name_values=metadata)  # NOQA
        return baseStorageUri + myblobname

    def delete(self, name):
        self.blob_service.delete_blob(self.container, name)

    def exists(self, name):
        try:
            self.blob_service.get_blob_properties(self.container, name)
            return True
        except Exception:
            return False

    def listdir(self, path):
        dirs = []
        files = []
        blobs = self.blob_service.list_blobs(self.container, prefix=(path or None))  # NOQA
        for blob in blobs:
            directory, file_name = os.path.split(blob.name)
            dirs.append(directory)
            files.append(file_name)
        return (dirs, files)

    def size(self, name):
        properties = self.blob_service.get_blob_properties(self.container, name)  # NOQA
        return properties.get('content-length')

    def url(self, name):
        blob = self.blob_service.list_blobs(self.container)
        return name

    def modified_time(self, name):
        metadata = self.blob_service.get_blob_metadata(self.container, name)
        modified_time = float(metadata.get('x-ms-meta-modified_time'))
        return datetime.fromtimestamp(modified_time)


def make_readable_name(name):
    name = name.replace(' ', '_')
    name = name[-30:]
    current = datetime.now()
    name = str(current.year) + '_' + str(current.month) + '_' + str(current.day) + '_' + str(current.hour) + '_' + str(current.minute) + '_' + str(current.second) + '_' + name    # NOQA
    return name
