import datetime
import json
from django.utils import timezone
from django.shortcuts import render, HttpResponseRedirect, redirect
from django.shortcuts import HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.files.base import ContentFile, File
from django.core.urlresolvers import reverse
from django.core.exceptions import MultipleObjectsReturned
from rest_framework_jwt.views import obtain_jwt_token
from django.views.generic import View
from django.db.models import Q
from django.contrib import messages
from .decorators import is_login, login_required
from .models import (
    User, Users, UserType, Children, Group,
    VinculationChild, ValidationIds, LinkedAccounts,
    LinkedAccountsOptions
)
from .forms import (
    EditAccountForm, InviteParentForm, LoginForm,
    RegisterForm, AddPatientForm
)
from .helpers import create_vinculationCode, create_linkId, generate_jwt
from levels.models import (
    ArbolmusicalLevels, ArbolmusicalHeaders,
    ArenamagicaHeaders, ArenamagicaLevels,
    DondequedolabolitaHeaders,
    RecolecciondeltesoroHeaders,
    RioHeaders,
    ShadowsHeaders,
    TowiIndex,
    PruebasLevelsNew,
    PruebasHeadersNew
)

REGISTRO = ['registro_familiar', 'registro_patients', 'registro_alumnos']
ERROR = 40
SUCCESS = 25


class Home(View):
    @login_required
    def get(self, request):
        template_name = 'index.html'
        return render(request, template_name)


class Login(View):
    def get(self, request):
        template_name = 'login1.html'
        loginForm = LoginForm()
        registerForm = RegisterForm()
        context = {
            'form': loginForm,
            'registerForm': registerForm
        }
        return render(request, template_name, context)

    def post(self, request):
        template_name = 'login1.html'
        data = request.POST
        action = request.POST['action']
        context = {
            'form': LoginForm(),
            'registerForm': RegisterForm()
        }
        if action == 'login':
            form = LoginForm(request.POST)
            if form.is_valid():
                user = authenticate(
                    username=form.cleaned_data['email'],
                    password=form.cleaned_data['password']
                )
                if user is None:
                    messages.error(request, 'Tú correo ó contraseña son incorrectos, intentalo de nuevo.')  # NOQA
                else:
                    login(request, user)
                    return redirect('/home/')
            else:
                if form.errors:
                    for field in form:
                        for error in field.errors:
                            messages.add_message(request, ERROR, error)
        elif action in REGISTRO:
            form = RegisterForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                if action == 'registro_familiar':
                    user_type = UserType.objects.get(description='Padre')
                elif action == 'registro_patients':
                    user_type = UserType.objects.get(description='Especialista')
                else:
                    user_type = UserType.objects.get(description='Especialista')
                cd['user_type'] = user_type
                try:
                    user = User.objects.create_user(
                        email=cd['email'],
                        username=cd['email'],
                        first_name=cd['first_name'],
                        last_name=cd['last_name'],
                        password=cd['password'],
                        user_type=cd['user_type']
                    )
                    related_user = user.create_game_user(**cd)
                    user.related_user = related_user
                    user.save()
                    vids = ValidationIds.objects.create(
                        pid=user.related_user.id,
                        vcode=create_vinculationCode(),
                        link_id=create_linkId()
                    )
                    login(request, user)
                    return redirect('/home/')
                except Exception as e:
                    if user:
                        user.delete()
                    messages.add_message(request, ERROR, 'Ocurrio un error inesperado intentalo mas tarde.')
            else:
                if form.errors:
                    for field in form:
                        for error in field.errors:
                            messages.add_message(request, ERROR, error)
        return render(request, template_name, context)


class DataInformation(View):
    @login_required
    def get(self, request):
        editAccountForm = EditAccountForm()
        template_name = 'admin-cuenta.html'
        ctx = {
            'editAccountForm': editAccountForm
        }
        return render(request, template_name, ctx)

    @login_required
    def post(self, request):
        data = request.POST
        action = data.get('action')
        if action == 'update_data':
            user = Users.objects.get(id=request.user.related_user.id)
            if self.validate_data(data.get('first_name', None), user.name):
                user.name = data.get('first_name')
            if self.validate_data(data.get('last_name', None), user.lastname):
                user.lastname = data.get('last_name')
            if self.validate_data(data.get('greeting', None), user.greeting):
                user.greeting = data.get('greeting')
            if self.validate_data(data.get('email', None), user.email):
                user.email = data.get('emails')
            if self.validate_data(data.get('genre', None), user.genre):
                user.genre = data.get('genre')
            if self.validate_data(data.get('specialty', None), user.specialty):
                user.specialty = data.get('specialty')
            if request.FILES.get('avatar') is not None:
                user.picture.save(
                    '{0}_perfil.jpg'.format(
                        user.name), File(
                            request.FILES.get('avatar')
                        )
                )
            user.save()
            messages.success(request, 'Actualizaste con éxito tu perfil.')
        return redirect('/information')

    def validate_data(self, data, field):
        empty_list = ['', ' ', None]
        if data not in empty_list and data != field:
            return True
        else:
            return False


class Patients(View):
    @login_required
    def get(self, request):
        template_name = 'pacientes.html'
        childrens = request.user.childrens.all()
        groups = Group.objects.filter(user=request.user)
        vinculated_childs, own_childs = get_vinculatedChilds(request.user.related_user.id)
        ctx = {
            'user': request.user,
            'childrens': childrens,
            'groups': groups,
            'childs': vinculated_childs
        }
        return render(request, template_name, ctx)

    @login_required
    def post(self, request):
        if 'eliminarPaciente' in request.POST:
            try:
                children = Children.objects.get(
                    id=request.POST.get('eliminarPaciente'),
                    user=request.user
                )
                l_options = LinkedAccountsOptions.objects.filter(cid=request.POST['eliminarPaciente'])
                l_options.delete()
                children.user = None
                children.parentid = -1
                children.save()
                messages.add_message(request, SUCCESS, 'Se elimino el paciente con exito.')
                return redirect('/patients')
            except Exception as e:
                messages.add_message(request, ERROR, 'No se pudo eliminar intente mas tarde.')
                return redirect('/patients')
        else:
        #     if 'groupId' in request.POST and int(request.POST.get('groupId')) > 0:  # NOQA
        #         groupData = Group.objects.get(id=request.POST.get('groupId'))
        #         childrens = groupData.children.all()
        #     else:
        #         childrens = user.childrens.all()
        #     ctx = {
        #         'user': user,
        #         'childrens': childrens,
        #         'groups': groups
        #     }
            messages.add_message(request, ERROR, 'No se pudo eliminar intente mas tarde.')
            return redirect('/patients')


class AddPatient(View):
    @login_required
    def get(self, request):
        template_name = 'agregarPaciente.html'
        user = request.user
        ctx = {
            'user': user,
            'childrens': user.childrens.all()
            }
        return render(request, template_name, ctx)


class RegisterPatient(View):
    @login_required
    def get(self, request):
        template_name = 'registrarPaciente.html'
        user = request.user
        ctx = {
            'user': user,
            }
        return render(request, template_name, ctx)

    @login_required
    def post(self, request):
        user = request.user
        name = request.POST.get('name', None)
        f_last = request.POST.get('f_last', None)
        s_last = request.POST.get('s_last', None)
        dob = request.POST.get('dob', None)
        gender = request.POST.get('gender', None)
        grade = request.POST.get('grade', None)
        image = request.FILES.get('image', None)
        option = request.POST.get('options', None)
        birthdate = datetime.datetime.strptime(dob, '%Y-%m-%d').date()

        if image is None:
            from django.conf import settings
            image = "{}.png".format(option)

        child = Children.objects.create(
            parentid=user.related_user.id,
            user=user,
            name=name,
            lastname="{} {}".format(f_last, s_last),
            dob=birthdate,
            grade=grade,
            genre=gender,
            picture=image,  # NOQA
            register_date=timezone.now(),
        )
        messages.add_message(request, SUCCESS, 'Se agrego el paciente con exito.')  # NOQA
        return redirect('/patients')


class followPatient(View):
    @login_required
    def get(self, request):
        template_name = 'seguirPaciente.html'
        form = AddPatientForm()
        user = request.user
        ctx = {
            'user': user,
            'form': form
        }
        return render(request, template_name, ctx)

    @login_required
    def post(self, request):
        form = AddPatientForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            vid = ValidationIds.objects.filter(
                pid=request.user.related_user.id
            )
            try:
                vad = ValidationIds.objects.get(
                    pid=Users.objects.get(email=cd['email']).id,
                    link_id=cd['link_id']
                )
            except MultipleObjectsReturned:
                vad = ValidationIds.objects.filter(
                    pid__in=Users.objects.filter(email=cd['email']),
                    link_id=cd['link_id']
                )
                if vid.exists() and vad.exists():
                    link_id = vid[0].link_id
                    linked = LinkedAccounts.objects.filter(
                        vinculation_code_u1=cd['link_id'],
                        vinculation_code_u2=link_id
                    )
                    if linked.exists():
                        linked_o = LinkedAccountsOptions.objects.create(
                            linked_account_id=linked[0].id,
                            auth=0,
                            cid=-1
                        )
                        messages.add_message(request, SUCCESS, 'Solicitud exitosa, espera que aprueben tu seguimiento.')  # NOQA

                    else:
                        linked = LinkedAccounts.objects.create(
                            vinculation_code_u1=cd['link_id'],
                            vinculation_code_u2=link_id,
                            auth_u1=0,
                            auth_u2=1
                        )
                        linked_o = LinkedAccountsOptions.objects.create(
                            linked_accounts_options=linked.id,
                            auth=0
                        )
                        messages.add_message(request, SUCCESS, 'Solicitud exitosa, espera que aprueben tu seguimiento.')  # NOQA
                else:
                    messages.add_message(request, ERROR, 'Usuario no encontrado, verifica los datos ingresados.')  # NOQA
                return redirect('/follow-patient')

        return redirect('/patients')


class Groups(View):
    @login_required
    def get(self, request):
        template_name = 'grupos.html'
        groups = Group.objects.filter(user=request.user)
        childs = []
        for group in groups:
            for child in group.children.all():
                childs.append(child.id)
        children = Children.objects.filter(
            user=request.user).exclude(
                id__in=childs
            )
        context = {
            'groups': groups,
            'childs': children
        }
        return render(request, template_name, context)

    @login_required
    def post(self, request):
        data = request.POST
        if 'registro' in data:
            try:
                group = Group.objects.create(
                    user=request.user,
                    name=request.POST.get('name')
                )
            except Exception as e:
                pass
        elif 'modificacionNombreGrupo' in data:
            if 'groupName' in request.POST:
                try:
                    group = Group.objects.get(id=data['modificacionNombreGrupo'])
                    group.name = data['groupName']
                    group.save()
                    messages.add_message(request, SUCCESS, 'Se modifico el grupo con exito!')  # NOQA
                    return redirect('/groups')
                except Exception as e:
                    messages.add_message(request, ERROR, 'Ocurrio un error inesperado intentalo mas tarde.')  # NOQA
                    return redirect('/groups')

        elif 'eliminarGrupoButton' in data:
            try:
                group = Group.objects.get(id=data['eliminarGrupoButton'])
                group.delete()
                messages.add_message(request, SUCCESS, 'Se elimino el grupo con exito!')  # NOQA
                return redirect('/groups')
            except Exception as e:
                messages.add_message(request, ERROR, 'Ocurrio un error inesperado intentalo mas tarde.')  # NOQA
                return redirect('/groups')

        elif 'agregarPaciente' in data:
            try:
                group = Group.objects.get(id=data['agregarPaciente'])
                children = Children.objects.filter(
                    id__in=request.POST.getlist('options')
                )
                group.children.add(*children)
                group.save()
                messages.add_message(request, SUCCESS, 'Se agregaron los patients al grupo con exito!')  # NOQA
                return redirect('/groups')
            except Exception as e:
                messages.add_message(request, ERROR, 'Ocurrio un error inesperado intentalo mas tarde.')  # NOQA
                return redirect('/groups')

        return HttpResponseRedirect('.')


class EditInfoPacient(View):
    @login_required
    def get(self, request, id):
        template_name = 'editar-info.html'
        child = Children.objects.get(pk=id)
        ctx = {
            'children': child,
            'user': request.user,
        }
        return render(request, template_name, ctx)

    @login_required
    def post(self, request, id):
        data = request.POST
        files = request.FILES
        action = data.get('action')
        if action == 'update_data':
            try:
                user = Children.objects.get(id=id)
                if data.get('name', None) != user.name:
                    user.name = data.get('name')
                lastname = data.get('f_last') + ' ' + data.get('s_last')
                if lastname != user.lastname:
                    user.lastname = lastname
                if data.get('gender', None) != user.genre:
                    user.genre = data.get('gender')
                if data.get('grade', None) != user.grade:
                    user.grade = data.get('grade')
                if data.get('diagnostic', None) != user.diagnostic:
                    user.diagnostic = data.get('diagnostic')
                if data.get('dob', None) != user.dob:
                    user.dob = data.get('dob')
                if 'picture' in files and files.get('picture', None) != user.picture:  # NOQA
                    user.picture = files.get('picture')
                if 'picture' not in files:
                    if 'options' in data:
                        user.picture = data.get('options')
                user.save()
                messages.add_message(request, SUCCESS, 'Has editado al usuario con éxito!')  # NOQA
            except Exception as e:
                messages.add_message(request, ERROR, 'Ocurrio un error inesperado, intentalo mas tarde.')  # NOQA

        return redirect('/patients')


class InviteParent(View):
    @login_required
    def get(self, request, id):
        template_name = 'invitacion.html'
        form = InviteParentForm()
        child = Children.objects.get(pk=id)
        ctx = {
            'children': child,
            'user': request.user,
            'form': form,
        }
        return render(request, template_name, ctx)

    @login_required
    def post(self, request, id):
        if 'inviteParent' in request.POST:
            form = InviteParentForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                try:
                    user = User.objects.get(
                        related_user__email=cd['email']
                    )
                except User.DoesNotExist:
                    user = User.objects.create(
                        password=cd['password'],
                        email=cd['email']
                    )
                vid = ValidationIds.objects.filter(pid=user.related_user.id)
                vid_request_user = ValidationIds.objects.filter(pid=request.user.related_user.id)
                if vid.exists() and vid_request_user.exists():
                    linked_account = LinkedAccounts.objects.filter(
                        vinculation_code_u2=vid[0].link_id,
                        vinculation_code_u1=vid_request_user[0].link_id,
                    )
                    if linked_account:
                        la_options = LinkedAccountsOptions.objects.get_or_create(
                            linked_account_id=linked_account[0].id,
                            cid=id,
                            auth=0
                        )
                    else:
                        linked_account = LinkedAccounts.objects.filter(
                            vinculation_code_u2=vid[0].link_id,
                            vinculation_code_u1=vid_request_user[0].link_id,
                            auth_u1=1,
                            auth_u2=0,
                        )
                    messages.add_message(request, SUCCESS, 'Vinculado con éxito, espera la confirmación.')
                    return redirect('/patients')

                else:
                    messages.add_message(request, ERROR, 'Ocurrio un error al vincular comunicate con nuestro staff.')
                    return redirect('/patients')

        return redirect('/patients')


class Invitations(View):
    @login_required
    def get(self, request):
        template_name = 'invitaciones.html'
        invitations = get_invitations(request.user.related_user.id)
        requests_inv, request_user = request_invitations(request.user.related_user.id)  # NOQA
        childs, own_childs = get_vinculatedChilds(request.user.related_user.id)
        ctx = {
            'invitations': invitations,
            'vinculated_childs': childs,
            'requests_inv': requests_inv,
            'requests_user': request_user,
            'own_childs': own_childs
        }
        return render(request, template_name, ctx)

    def post(self, request):
        data = request.POST
        if 'aceptar' in data:
            vid = ValidationIds.objects.filter(
                pid=request.user.related_user.id
            )
            if vid.exists():
                link_id = vid[0].link_id
                vinculated_childs = LinkedAccounts.objects.filter(
                    vinculation_code_u2=link_id,
                )
                if vinculated_childs.exists():
                    la_options = LinkedAccountsOptions.objects.get(
                        linked_account_id=vinculated_childs[0].id,
                        cid=data['aceptar']
                    )
                    vinculated_childs[0].auth_u2 = 1
                    la_options.auth = 1
                    vinculated_childs[0].save()
                    la_options.save()
                    messages.add_message(request, SUCCESS, 'Invitacion aceptada con exito.')
                    return redirect('/patients')
        elif 'rechazar' in data:
            vid = ValidationIds.objects.filter(
                pid=request.user.related_user.id
            )
            if vid.exists():
                link_id = vid[0].link_id
                vinculated_childs = LinkedAccounts.objects.filter(
                    vinculation_code_u2=link_id,
                )
                if vinculated_childs.exists():
                    la_options = LinkedAccountsOptions.objects.get(
                        linked_account_id=vinculated_childs[0].id,
                        cid=data['rechazar']
                    )
                    la_options.delete()
                    messages.add_message(request, SUCCESS, 'Invitacion rechazada con exito.')
                    return redirect('/patients')
        elif 'desvincular' in data:
            vid = ValidationIds.objects.filter(
                pid=request.user.related_user.id
            )
            if vid.exists():
                try:
                    link_id = vid[0].link_id
                    vinculated_childs = LinkedAccounts.objects.filter(
                        vinculation_code_u2=link_id,
                    )
                    if vinculated_childs.exists():
                        la_options = LinkedAccountsOptions.objects.get(
                            linked_account_id=vinculated_childs[0].id,
                            cid=data['desvincular']
                        )
                        la_options.delete()
                        messages.add_message(request, SUCCESS, 'Desvinculación con exito.')
                        return redirect('/patients')
                except Exception as e:
                    messages.add_message(request, ERROR, 'Ocurrio un error inesperado intentalo nuevamente ó comunicate con nosotros.')
                    return redirect('/invitaciones')
        elif 'aceptar-seguimiento' in data:
            return redirect('/seguimiento?id={}&user_id={}'.format(data['id_inv'], data['aceptar-seguimiento']))


class Seguimiento(View):
    @login_required
    def get(self, request):
        template_name = 'Seguimiento.html'
        childrens = request.user.childrens.all()
        ctx = {
            'user': request.user,
            'childrens': childrens,
            'vinculation_id': request.GET['id']
        }
        return render(request, template_name, ctx)

    @login_required
    def post(self, request):
        ids = request.POST.getlist('id')
        data = request.POST
        if ids and 'vinid' in data:
            linked_o = LinkedAccountsOptions.objects.get(pk=data['vinid'])
            linked_o.cid = int(ids.pop())
            linked_o.auth = 1
            linked_o.save()
            if ids:
                for id in ids:
                    LinkedAccountsOptions.objects.create(
                        linked_account_id=linked_o.linked_account_id,
                        cid=id,
                        auth=1
                    )
                    print('creada')
                    messages.add_message(request, SUCCESS, 'Vinculacion exitosa!')
                    return redirect('/invitaciones')
            else:
                messages.add_message(request, SUCCESS, 'Vinculacion exitosa!')
                return redirect('/invitaciones')


class Reports(View):
    @login_required
    def get(self, request, id):
        template_name = 'reportes.html'
        child = Children.objects.get(pk=id)
        try:
            vid = ValidationIds.objects.get(pid=request.user.related_user.id)
        except MultipleObjectsReturned:
            vid = ValidationIds.objects.filter(pid=request.user.related_user.id)[0]  # NOQA
        except Exception:
            vid = None
        from itertools import chain
        from operator import attrgetter

        arbolmusicalHeaders = ArbolmusicalHeaders.objects.filter(cid=child.id)
        arenaMagicaHeaders = ArenamagicaHeaders.objects.filter(cid=child.id)
        dqlbHeaders = DondequedolabolitaHeaders.objects.filter(cid=child.id)
        rdtHeaders = RecolecciondeltesoroHeaders.objects.filter(cid=child.id)
        rioHeaders = RioHeaders.objects.filter(cid=child.id)
        shadowHeaders = ShadowsHeaders.objects.filter(cid=child.id)

        result_list = sorted(
            chain(
                arbolmusicalHeaders,
                arenaMagicaHeaders,
                dqlbHeaders,
                # rdtHeaders,
                rioHeaders,
                shadowHeaders
            ),
            key=attrgetter('date'),
            reverse=False)

        dates = set(obj.date.date() for obj in result_list if obj.date)
        days_list = []
        count = 0
        for date in dates:
            count += 1
            games = sorted(
                chain(
                    arbolmusicalHeaders.filter(date__startswith=date),
                    arenaMagicaHeaders.filter(date__startswith=date),
                    dqlbHeaders.filter(date__startswith=date),
                    rdtHeaders.filter(date__startswith=date),
                    rioHeaders.filter(date__startswith=date),
                    shadowHeaders.filter(date__startswith=date)
                ),
                key=attrgetter('date', 'gamekey'),
                reverse=True)
            days_list.append(DaySession(
                date,
                games,
                count
                )
            )
        days_list.sort(key=lambda r: r.date, reverse=True)
        paginator = Paginator(days_list, 5)
        page = request.GET.get('page')
        try:
            days = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            days = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            days = paginator.page(paginator.num_pages)

        ctx = {
            'children': child,
            'user': request.user,
            'vid': vid,
            'days': days,
        }
        return render(request, template_name, ctx)


class AccountInfo(View):
    @login_required
    def get(self, request):
        template_name = 'editar-info.html'
        ctx = {
            'user': request.user
        }
        return render(request, template_name, ctx)


def get_vinculatedChilds(uid):
    vid = ValidationIds.objects.filter(
        pid=uid
    )
    childs, own_childs = [], []
    if vid.exists():
        link_id = vid[0].link_id
        vinculated_childs = LinkedAccounts.objects.filter(
            vinculation_code_u2=link_id,
            auth_u1=1,
            auth_u2=1
        )
        own_vinculated_childs = LinkedAccounts.objects.filter(
            vinculation_code_u1=link_id,
            auth_u1=1,
            auth_u2=1
        )
        if vinculated_childs.exists():
            childsIds = LinkedAccountsOptions.objects.filter(
                linked_account_id=vinculated_childs[0].id,
                auth=1
            ).values('cid')
            childs = Children.objects.filter(id__in=childsIds)
        if own_vinculated_childs.exists():
            ownchildsIds = LinkedAccountsOptions.objects.filter(
                linked_account_id=own_vinculated_childs[0].id,
                auth=1
            ).values('cid')
            own_childs = Children.objects.filter(id__in=ownchildsIds)
        return childs, own_childs
    else:
        return childs, own_childs


def get_invitations(uid):
    vid = ValidationIds.objects.filter(
        pid=uid
    )
    if vid.exists():
        link_id = vid[0].link_id
        vinculated_childs = LinkedAccounts.objects.filter(
            vinculation_code_u2=link_id,
        )
        if vinculated_childs.exists():
            childsIds = LinkedAccountsOptions.objects.filter(
                linked_account_id=vinculated_childs[0].id,
                auth=0
            ).values('cid')
            childs = Children.objects.filter(id__in=childsIds)
            return childs
        else:
            return []
    else:
        return []


def request_invitations(uid):
    vid = ValidationIds.objects.filter(
        pid=uid
    )
    if vid.exists():
        link_id = vid[0].link_id
        linked_accounts = LinkedAccounts.objects.filter(
            vinculation_code_u1=link_id,
        )
        if linked_accounts.exists():
            requests_lo = LinkedAccountsOptions.objects.filter(
                linked_account_id__in=linked_accounts,
                cid=-1
            )
            request_user = User.objects.filter(
                related_user__id=ValidationIds.objects.filter(
                    link_id__in=linked_accounts.values('vinculation_code_u2')
                ).values('pid')
            )
            return requests_lo, request_user
        else:
            return [], []
    else:
        return [], []


def email_exists(request):
    from django.http import JsonResponse
    if request.method == 'GET':
        email = request.GET.get('email', None)
        data = {
            'is_taken': Users.objects.filter(email=email).exists()
        }
        return JsonResponse(data)
    else:
        return None


class DaySession(object):
    def __init__(self, date, games, count):
        self.date = date
        self.games = games
        self.count = count


def get_graph(request):
    from django.http import JsonResponse

    child_id = request.GET.get('child_id', None)
    towiIndex = TowiIndex.objects.filter(cid=child_id)
    labels = [ti.date.date().strftime("%s") for ti in towiIndex.order_by('-date')]  # NOQA
    maxValue = labels[0]
    minValue = labels[-1]

    am_ti = [{'x': str(ti.date.date().strftime("%s")), 'y': int(ti.index)} for ti in towiIndex.filter(  # NOQA
        cid=child_id,
        gamekey='ArbolMusical'
    ).order_by('date')]
    arm_ti = [{'x': str(ti.date.date().strftime("%s")), 'y': int(ti.index)} for ti in towiIndex.filter(  # NOQA
        cid=child_id,
        gamekey='ArenaMagica'
    ).order_by('date')]
    dqlb_ti = [{'x': str(ti.date.date().strftime("%s")), 'y': int(ti.index)} for ti in towiIndex.filter(  # NOQA
        cid=child_id,
        gamekey='DondeQuedoLaBolita'
    ).order_by('date')]
    rdt_ti = [{'x': str(ti.date.date().strftime("%s")), 'y': int(ti.index)} for ti in towiIndex.filter(  # NOQA
        cid=child_id,
        gamekey='Tesoro'
    ).order_by('date')]
    rio_ti = [{'x': str(ti.date.date().strftime("%s")), 'y': int(ti.index)} for ti in towiIndex.filter(  # NOQA
        cid=child_id,
        gamekey='Rio'
    ).order_by('date')]
    sh_ti = [{'x': str(ti.date.date().strftime("%s")), 'y': int(ti.index)} for ti in towiIndex.filter(  # NOQA
        cid=child_id,
        gamekey='JuegoDeSombras'
    ).order_by('date')]

    data = {
        'am_ti': am_ti,
        'arm_ti': arm_ti,
        'dqlb_ti': dqlb_ti,
        'rdt_ti': rdt_ti,
        'rio_ti': rio_ti,
        'sh_ti': sh_ti,
        'maxValue': maxValue,
        'minValue': minValue,
        'success': True
    }
    return JsonResponse(data)


def reports_redirect(request, id):
    response = redirect('http://187.248.54.146:6006/pruebas/{}/'.format(id))
    token = generate_jwt(request.user)
    response['token'] = token
    return response
