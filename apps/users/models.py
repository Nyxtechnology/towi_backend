from __future__ import unicode_literals
import uuid
import hashlib
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils import timezone

from django.utils import timezone
from django.db import models
from datetime import date
from .managers import UserManager


class UserSession(models.Model):
    user_id = models.IntegerField()
    session_type = models.IntegerField()
    date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'user_session'


class UserType(models.Model):  # Padre, especualista
    description = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'user_type'


class Users(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    lastname = models.CharField(max_length=100, null=True, blank=True)
    country = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=100, null=True, blank=True)
    genre = models.CharField(max_length=100, null=True, blank=True)
    relation = models.CharField(max_length=100, null=True, blank=True)
    email = models.CharField(max_length=100, blank=True)
    password = models.CharField(max_length=100, null=True, blank=True)
    picture = models.ImageField(
        null=True,
        blank=True)
    dob = models.DateField(null=True, blank=True)
    verified = models.IntegerField(null=True, blank=True)
    register_date = models.DateTimeField(
        default=timezone.now,
        null=True,
        blank=True
    )
    fb_id = models.CharField(max_length=200)
    user_type = models.IntegerField(null=True, blank=True)
    account_type = models.IntegerField(null=True, blank=True)
    specialty = models.CharField(null=True, blank=True, max_length=100)
    greeting = models.CharField(null=True, blank=True, max_length=100)

    @property
    def get_full_name(self):
        return "{} {}".format(self.name, self.lastname)

    @property
    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    class Meta:
        db_table = 'users'

    def __str__(self):
        return '{} {}'.format(self.name, self.lastname)


class User(AbstractBaseUser, PermissionsMixin):
    objects = UserManager()
    related_user = models.OneToOneField(Users, null=True, blank=True, related_name='usuarios')  # NOQA
    user_type = models.ForeignKey(UserType, null=True, blank=False, related_name='type')  # NOQA
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=30, blank=True, verbose_name='Nombre')  # NOQA
    last_name = models.CharField(max_length=30, blank=True, verbose_name='Apellidos')  # NOQA
    date_joined = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de Registro')  # NOQA
    is_active = models.BooleanField(default=True, verbose_name='Activo')
    is_staff = models.BooleanField(default=True, verbose_name='Es staff')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'Authenticate user'
        verbose_name_plural = 'Authenticate users'

    @property
    def get_full_name(self):
        return "{} {}".format(self.first_name, self.lastname)

    @property
    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    @classmethod
    def create_game_user(self, email, password, **kwargs):
        '''Creates and save the user for the game usage'''
        password2 = hashlib.md5()
        password2.update(password.encode('utf8'))
        password2 = password2.hexdigest()
        _user_cls = Users.objects.get_or_create(
            email=email,
            password=password2,
            name=kwargs['first_name'],
            lastname=kwargs['last_name'],
            user_type=kwargs['user_type'].id
        )[0]
        _user_cls
        return _user_cls


class Children(models.Model):
    parentid = models.IntegerField()
    user = models.ForeignKey(User, null=True, blank=False, related_name='childrens')  # NOQA
    name = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    dob = models.DateField()
    grade = models.CharField(max_length=100)
    genre = models.CharField(max_length=100)
    school = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    picture = models.ImageField(
        null=True,
        blank=True)
    register_date = models.DateTimeField()
    active = models.BooleanField(default=False)
    trial_active = models.BooleanField(default=False)
    diagnostic = models.TextField(null=True)

    class Meta:
        db_table = 'children'

    def __str__(self):
        return "{} {}".format(self.name, self.lastname)

    @property
    def apellido_paterno(self):
        if self.lastname:
            return self.lastname.split(' ')[0]
        else:
            return ""

    @property
    def apellido_materno(self):
        if self.lastname:
            try:
                return self.lastname.split(' ')[1]
            except Exception:
                return ""
        else:
            return ""

    @property
    def full_name(self):
        return '{} {}'.format(self.name, self.lastname)

    @property
    def age(self):
        today = date.today()
        return today.year - self.dob.year - ((today.month, today.day) < (self.dob.month, self.dob.day))


class Group(models.Model):
    user = models.ForeignKey(User, null=True, blank=False)
    name = models.CharField(null=False, blank=False, max_length=100)
    children = models.ManyToManyField(Children, blank=True)

    def __str__(self):
        return "{}".format(self.name)


# class Vinculation(models.Model):  # no se usa
#     parentid = models.IntegerField()
#     cid = models.IntegerField()
#     vinculation_code = models.IntegerField()
#
#     class Meta:
#         managed = False
#         db_table = 'vinculation'


class LinkedAccounts(models.Model):
    ''' MUESTRA EL LINK_ID DE LA PERSONA SOLICITANDO LA VINCULACION CON LA QUE QUIERE VINCULAR
        SE CREA AL VINCULAR EL USUARIO CON LAS AUTORIZACIONES DE AMBAS PARTES
        SI LA AUTORIZACION NO ES DE AMBAS PARTES NO ES POSIBLE HACER LA VINCULACION
    '''
    vinculation_code_u1 = models.CharField(max_length=10)  # LINK ID DE ValidationIds
    vinculation_code_u2 = models.CharField(max_length=10)
    auth_u1 = models.IntegerField()  # boolean field
    auth_u2 = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'linked_accounts'


class LinkedAccountsOptions(models.Model):
    '''
        ES LA TABLA PARA VINCULAR AL NIÑO CON UNA AUTORIZACION(linkedaccounts)
        UNO A MUCHOS NIÑOS
        EL CAMPO AUTH ES PARA Desvincular A UN NIÑO
    '''
    linked_account_id = models.IntegerField()
    cid = models.IntegerField()
    auth = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'linked_accounts_options'


class ValidationIds(models.Model):
    ''' SE CREA AL CREAR UN USUARIO
    VCODE ES UN CODIGO RANDOM
    LINK_ID ES UN CODIGO RANDOM
    '''
    pid = models.CharField(max_length=11)
    vcode = models.CharField(max_length=500)  # codigo random VERIFICAR EL CORREO UNICAMENTE 40 caracteres
    link_id = models.CharField(max_length=10)  # PXLCODIGO RANDOM CON ESTE SE VINCULA 5 digitos despues de pxl

    class Meta:
        managed = False
        db_table = 'validation_ids'


class VinculationChild(models.Model):
    owner_user = models.ForeignKey(User, related_name='owner_user')
    linked_user = models.ForeignKey(User, related_name='linked_user')
    children = models.ForeignKey(Children)

    class Meta:
        verbose_name = 'Usuarios vinculados'
