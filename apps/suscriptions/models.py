from django.db import models


class Apikeys(models.Model):
    parentid = models.IntegerField()
    key = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'apikeys'


class ChildrenSuscription(models.Model):
    cid = models.IntegerField(unique=True)
    suscription_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'children_suscription'


class InteresadosEnSuscribirse(models.Model):  #checar modelo posiblemente no se use
    email = models.CharField(max_length=200)
    date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'interesados_en_suscribirse'


class RegisteredSerialCodes(models.Model): # ya no se usa
    serial = models.CharField(max_length=100)
    md5_device = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'registered_serial_codes'


class Feed(models.Model):
    uid = models.CharField(max_length=11)
    target = models.CharField(max_length=11)
    mssg = models.CharField(max_length=1000)
    thread = models.IntegerField()
    type = models.CharField(max_length=100)
    date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'feed'


class FreeTrial(models.Model):
    md5_device = models.CharField(max_length=100)
    trial_id = models.DateTimeField()
    linked_account = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'free_trial'


class SerialCodesPc(models.Model):  # no se usa
    pid = models.IntegerField()
    serial = models.CharField(max_length=100)
    mail = models.CharField(max_length=100)
    date = models.DateTimeField()
    id_bw = models.CharField(max_length=100)
    card_bw = models.CharField(max_length=100)
    ord_id_bw = models.CharField(max_length=100)
    auth_code_bw = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'serial_codes_pc'


class AccountInfo(models.Model):
    pid = models.IntegerField()
    acc_type = models.CharField(max_length=100)
    activation_code = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'account_info'


class AccountType(models.Model): #vinculado, suscripcion
    description = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'account_type'
